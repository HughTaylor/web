module.exports.resizeImage = resizeImage;
const { exec } = require("child_process");
const Joi = require('joi');
const wget = require('wget-improved');
const sharp = require('sharp');
const http = require('http');
const https = require('https');
const url = require('url');

const schema = Joi.object({
  iurl: Joi.string()
        .uri()
});



function resizeImage(req, res) {

  // check the data submitted by the user against the joi schema
  const validatedUrl = schema.validate({ iurl: req.body.iurl });

  // if there's a JOI error, 
  if (validatedUrl.error) {

    // log it, set the error, and render the page
    console.log(validatedUrl.error);
    renderPage(res, "", "Pease enter a valid URL");
  } else {  
    // download it
    // the next line doesn't work for some reason.
    //const src = JSON.stringify(validatedUrl.value.iurl);
    const src = req.body.iurl;

    // check the header first
//    checkHeader(req, res, src);
    
//    checking header skipped because not all responses contained correctly formatted header
    downloadImage(req, res, src);
  }
}



// request the header from the target url, and check the filesize
function checkHeader(preq, pres, src) {
  let srcUrl, hreq;
  srcUrl = url.parse(src);
  if (!srcUrl.protocol || srcUrl.protocol === null) {
    renderPage(pres, "", "Unknown request protocol");
  }
  hreq = https.request(
    {
      protocol: srcUrl.protocol,
      host: srcUrl.hostname,
      port: srcUrl.port,
      path: srcUrl.pathname + (srcUrl.search || ''),
      //proxy
      //auth
      method: 'HEAD'
    },
    function(hres) {
      let fileSize;
      // if we get a 200 response
      if (hres.statusCode === 200) {
        // check the filesize
        fileSize = Number(hres.headers['content-length']);
        if (isNaN(fileSize) === true) {
	  renderPage(pres, "", "File error, no target file size in response header");
        } else if (fileSize > 10000000) {
	  renderPage(pres, "", "File error, target file too large (>10MB)");
	} else {
	  // download it if it's between 0 and 10MB
	  downloadImage(preq, pres, src);
	}
      } else { // if we did not get a 200 response
	renderPage(pres, "", "Target server responded with bad response code");
      } 
    } //callback function
  );//req args
  hreq.end()
}



function downloadImage(req, res, src) {
    const output = './resizer/tmp';
    var downloadError = null;

    // wget.download(src, output, options);
    let download = wget.download(src, output);

    download.on('error', function(err) {
      console.log(err);
      downloadError = err;
      renderPage(res, "", "Error downloading image");
    });

    download.on('start', function(fileSize) {
      console.log(fileSize);
    });

    download.on('end', function(output) {
      console.log(output);
      if(downloadError) {
        renderPage(res, "", "Could not get file information");
      } else {
        processImage(req, res);
      }
    });

    download.on('progress', function(progress) {
      typeof progress === 'number'
       // code to show progress bar
    });
}



function processImage(req, res) {
  sharp('./resizer/tmp')
    .flatten({ 
      background: { r: 255, g: 255, b: 255 }
    })
    .resize(160, 160, {
      fit: 'contain',
      background: { r: 255, g: 255, b: 255 }
    })
    .toFile('./public/images/output.jpg', (err, info) => {
      if(err) {
        renderPage(res, "", "Error resizing image");
      } else {
        renderPage(res, "images/output.jpg", "");
      }
  });
}





function renderPage(res, result, error) {
res.render('pages/resizer', {
  result: result,
  error: error
});
}
